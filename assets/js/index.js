// HOME

const GetDataHome = (homeURL, homeDiv) => {
  fetch(`https://api.themoviedb.org/3/movie/${homeURL}?api_key=${apikey}`)
    .then((reponse) => reponse.json())
    .then((transformation) => {
      const homeResult = transformation.results;
      const wid = 160 * homeResult.length;
      homeDiv.style.height = ('230px');
      homeDiv.style.width = (`${wid}px`);

      for (let i = 0; i < homeResult.length; i += 1) {
        const hPoster = homeResult[i].poster_path;
        const hTitle = homeResult[i].title;
        const hMovieId = homeResult[i].id;
        const hMovie = document.createElement('div');
        hMovie.style.width = '160px';
        hMovie.style.display = 'inline-block';
        hMovie.style.position = 'relative';
        homeDiv.append(hMovie);
        const hA = document.createElement('a');
        hA.id = hMovieId;
        hA.style.position = 'absolute';
        hA.style.left = '0';
        hA.style.right = '0';
        hA.style.zIndex = 999;
        hA.style.width = '160px';
        hA.style.height = '230px';
        hA.href = './movies.html';
        hMovie.append(hA);
        const hMoviePoster = document.createElement('img');
        hMoviePoster.setAttribute('src', `https://image.tmdb.org/t/p/w154${hPoster}`);
        hMoviePoster.setAttribute('alt', `Poster - ${hTitle}`);
        hMovie.append(hMoviePoster);
      }
    });
}

// Now playing movies

const urlPlaying = 'now_playing';
const playingDiv = document.getElementById('playing-container');

GetDataHome(urlPlaying, playingDiv);

// Upcoming movies
const urlUpcoming = 'upcoming';
const upcomingDiv = document.getElementById('upcoming-container');

GetDataHome(urlUpcoming, upcomingDiv);

// Top rated movies
const urlTop = 'top_rated';
const topDiv = document.getElementById('top-rated-container');

GetDataHome(urlTop, topDiv);

// Popular
const urlPop = 'popular';
const popDiv = document.getElementById('popular-container');

GetDataHome(urlPop, popDiv);

// SEARCH

// Barre de recherche & boutons de pagination

const searchButton = document.getElementById('search');
const inputField = document.getElementById('search-input');
const nextItem = document.getElementById('page-item-next');
const nextButton = document.getElementById('next-btn');
const prevItem = document.getElementById('page-item-prev');
const prevButton = document.getElementById('prev-btn');

// FONCTION AFFICHAGE : POUR AFFICHER LES DONNÉES RÉCUPÉRÉES

const affichage = (data) => {
  const homeContent = document.getElementById('content-home');
  homeContent.style.display = 'none';
  const searchContent = document.getElementById('content-search');
  searchContent.style.display = 'block';
  const resultContainer = document.getElementById('search-results');
  resultContainer.innerHTML = ''; // vide le contenu html du container à chaque recherche

  // phrase d'introduction pour les résultats de la recherche:
  const introSearch = document.getElementById('search-intro');
  if (inputField.value !== '') {
    introSearch.innerText = `We found ${data.total_results} results for "${inputField.value}" :`;
  }

  for (let i = 0; i < data.results.length; i += 1) {
    // récupération des éléments du json
    const poster = data.results[i].poster_path;
    const titre = data.results[i].title;
    const year = new Date(data.results[i].release_date).getFullYear();
    const movieId = data.results[i].id;
    const voteAverage = data.results[i].vote_average;
    const voteCount = data.results[i].vote_count;

    // création d'un container pour chaque film
    const movie = document.createElement('div');
    movie.classList.add('d-flex');
    movie.classList.add('my-1');
    resultContainer.append(movie);

    // poster
    const posterDiv = document.createElement('div');
    posterDiv.style.width = '60px'; // en cas d'absence de poster
    posterDiv.style.minHeight = '90px'; // en cas d'absence de poster
    posterDiv.classList.add('mr-3');
    posterDiv.classList.add('flex-shrink-0');
    movie.append(posterDiv);

    if (poster === null) { // en cas d'absence de poster
      posterDiv.style.backgroundColor = 'lightgray';
    } else {
      const moviePoster = document.createElement('img');
      moviePoster.style.width = '100%';
      moviePoster.setAttribute('src', `https://image.tmdb.org/t/p/w92${poster}`);
      moviePoster.setAttribute('alt', `Poster - ${titre}`);
      posterDiv.append(moviePoster);
    }

    // titre et année
    const titleDiv = document.createElement('div');
    movie.append(titleDiv);
    const movieTitle = document.createElement('a');
    movieTitle.href = './movies.html';
    movieTitle.id = movieId;
    movieTitle.classList.add('text-info');
    movieTitle.classList.add('link-to-details');

    if (data.results[i].release_date === '' || !data.results[i].release_date) {
      movieTitle.innerText = titre;
    } else {
      movieTitle.innerText = `${titre} (${year})`;
    }
    titleDiv.append(movieTitle);

    // ratings
    const movieVote = document.createElement('p');
    movieVote.classList.add('text-muted');
    if (data.results[i].vote_average) {
      movieVote.innerText = `${voteAverage} / 10 (${voteCount} users)`;
      movieVote.style.fontSize = '12px';
    }
    titleDiv.append(movieVote);
  }
};

// FONCTION GET DATA : FETCH LES DONNÉES

const getData = (pageNumber) => {
  const urlSearch = `https://api.themoviedb.org/3/search/movie?api_key=${apikey}&query=${inputField.value}&page=`;
  fetch(urlSearch + pageNumber)
    .then((reponse) => reponse.json())
    .then((transformation) => {
      const searchResult = transformation;
      const totalPages = searchResult.total_pages;

      // Pagination indice
      document.getElementById('page-and-total').innerText = `${pageNumber} / ${totalPages}`;

      // Pagination min
      if (pageNumber > 1) {
        prevItem.classList.remove('disabled');
        prevButton.removeAttribute('aria-disabled', 'true');
        prevButton.classList.add('text-info');
        prevButton.classList.remove('text-muted');
      } else {
        prevItem.classList.add('disabled');
        prevButton.setAttribute('aria-disabled', 'true');
        prevButton.classList.remove('text-info');
        prevButton.classList.add('text-muted');
      }

      // Pagination max
      if (pageNumber < totalPages) {
        nextItem.classList.remove('disabled');
        nextButton.removeAttribute('aria-disabled', 'true');
        nextButton.classList.add('text-info');
        nextButton.classList.remove('text-muted');
      } else {
        nextItem.classList.add('disabled');
        nextButton.setAttribute('aria-disabled', 'true');
        nextButton.classList.remove('text-info');
        nextButton.classList.add('text-muted');
      }

      // appel de la fonction affichage
      affichage(searchResult);
    });
}

// SEARCH & PAGINATION BUTTONS
let currentPage = 1;

searchButton.addEventListener('click', (event) => { // quand on clique sur le bouton search
  event.preventDefault(); // evite que la page se rafraichisse avec bouton de type submit
  currentPage = 1;
  getData(currentPage);
});

nextButton.addEventListener('click', () => {
  currentPage += 1;
  getData(currentPage);
});

prevButton.addEventListener('click', () => {
  if (currentPage > 1) {
    currentPage -= 1;
    getData(currentPage);
  }
});

// LOCAL STORAGE
document.addEventListener('click', (myEvent) => { // quand on clique sur le titre d'un film, store l'id
  localStorage.setItem('id', myEvent.target.id);
});
