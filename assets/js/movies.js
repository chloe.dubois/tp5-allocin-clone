// récupère l'identifiant du film stoché dans le Local Storage
const movieId = localStorage.getItem('id');

const urlMovie = `https://api.themoviedb.org/3/movie/${movieId}?api_key=${apikey}&append_to_response=credits,images,keywords,recommendations`;

// FONCTION AFFICHAGE CASTING
const affichageCast = (movieCastI) => {
  const profile = movieCastI.profile_path;
  if (profile) {
    const roleDiv = document.createElement('div');
    roleDiv.setAttribute('itemprop', 'actor');
    roleDiv.setAttribute('itemscope', '');
    roleDiv.setAttribute('itemtype', 'http://schema.org/PerformanceRole');
    roleDiv.classList.add('row');
    roleDiv.classList.add('my-1');
    roleDiv.classList.add('align-items-center');
    document.getElementById('movie-cast').append(roleDiv);

    const profileDiv = document.createElement('div');
    profileDiv.classList.add('col-sm-1');
    roleDiv.append(profileDiv);

    const profileImg = document.createElement('img');
    profileImg.setAttribute('src', `https://image.tmdb.org/t/p/w45${profile}`);
    profileImg.setAttribute('alt', movieCastI.name);
    profileDiv.append(profileImg);

    const actorP = document.createElement('p');
    actorP.setAttribute('itemprop', 'actor');
    actorP.setAttribute('itemscope', '');
    actorP.setAttribute('itemtype', 'http://schema.org/Person');
    actorP.classList.add('col-sm-5');
    actorP.classList.add('m-0');
    roleDiv.append(actorP);

    const actorName = document.createElement('span');
    actorName.setAttribute('itemprop', 'name');
    actorName.innerText = movieCastI.name;
    actorP.append(actorName);

    const characterName = document.createElement('p');
    characterName.setAttribute('itemprop', 'characterName');
    characterName.innerText = movieCastI.character;
    characterName.classList.add('font-italic');
    characterName.classList.add('text-black-50');
    characterName.classList.add('col-sm-6');
    characterName.classList.add('m-0');
    roleDiv.append(characterName);
  }
};

const affichageDetails = (data) => {
  // Titre et date
  const { title } = data;
  document.getElementById('movie-title').innerText = title;

  const year = new Date(data.release_date).getFullYear();
  if (data.release_date && data.release_date !== '') {
    document.getElementById('movie-year').innerText = `(${year})`;
  }

  // Duration
  document.getElementById('movie-duration').innerText = `${data.runtime} min`;
  document.getElementById('movie-duration-2').innerText = `${data.runtime} min`;

  // Genre
  const genres = [];
  data.genres.forEach((entry) => {
    genres.push(entry.name);
  });
  document.getElementById('movie-genre').innerText = genres.join(', ');
  document.getElementById('movie-genre-2').innerText = genres.join(', ');

  // Date complète
  const date = new Date(data.release_date);
  const monthInLetters = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  const month = monthInLetters[date.getMonth()];
  const day = date.getDate();
  document.getElementById('movie-date').innerText = `${day} ${month} ${year}`;
  document.getElementById('movie-date-2').innerText = `${day} ${month} ${year}`;

  // carousel

  const carousel = document.getElementById('carousel');
  const images = data.images.backdrops;

  const backdrop = data.backdrop_path;
  const carouselItem1 = document.getElementById('carousel-item-1');
  if (backdrop) {
    carouselItem1.setAttribute('src', `https://image.tmdb.org/t/p/w1280${backdrop}`);
    carouselItem1.setAttribute('alt', title);
  }

  for (let i = 1; i < images.length; i += 1) {
    const imgPath = images[i].file_path;
    const imgDiv = document.createElement('div');
    imgDiv.classList.add('carousel-item');
    carousel.append(imgDiv);
    const image = document.createElement('img');
    image.setAttribute('src', `https://image.tmdb.org/t/p/w1280${imgPath}`);
    image.setAttribute('alt', title);
    image.classList.add('d-block');
    image.classList.add('w-100');
    imgDiv.append(image);
  }

  // Keywords
  const keyw = [];
  data.keywords.keywords.forEach((entry) => {
    keyw.push(entry.name);
  });
  document.getElementById('movie-keywords').innerText = keyw.join(', ');

  // Director(s)
  const directors = [];
  data.credits.crew.forEach((entry) => {
    if (entry.job === 'Director') {
      directors.push(entry.name);
    }
  });
  document.getElementById('movie-director').innerText = directors.join(', ');

  // Writer(s)
  const writers = [];
  data.credits.crew.forEach((entry) => {
    if (entry.job === 'Screenplay' || entry.job === 'Writer' || entry.job === 'Novel') {
      writers.push(entry.name);
    }
  });
  document.getElementById('movie-writer').innerText = writers.join(', ');

  // Taglines
  document.getElementById('movie-tagline').innerText = data.tagline;

  // Stars
  const stars = [];
  const movieCast = data.credits.cast;
  for (let i = 0; i < 4; i += 1) {
    stars.push(movieCast[i].name);
  }
  const movieStars = stars.join(', ');
  document.getElementById('movie-stars').innerText = movieStars;

  // Cast : appel de la fonction affichage casting
  if (movieCast.length < 20) {
    for (let i = 0; i < movieCast.length; i += 1) {
      affichageCast(movieCast[i]);
    }
  } else {
    for (let i = 0; i < 20; i += 1) {
      affichageCast(movieCast[i]);
    }
  }

  // Spoken Languages
  const languages = [];
  data.spoken_languages.forEach((entry) => {
    languages.push(entry.name);
  });
  document.getElementById('movie-languages').innerText = languages.join(', ');

  // Storyline
  document.getElementById('movie-storyline').innerText = data.overview;

  // Ratings
  document.getElementById('movie-rating').innerText = data.vote_average;
  document.getElementById('movie-vote').innerText = data.vote_count;

  // Budget
  document.getElementById('movie-budget').innerText = `$${data.budget}`;

  // Production cie & countries
  const movieCie = document.getElementById('movie-cies');
  const companies = data.production_companies;

  for (let i = 0; i < companies.length; i += 1) {
    const cieDiv = document.createElement('p');
    cieDiv.setAttribute('itemprop', 'productionCompany');
    cieDiv.setAttribute('itemscope', '');
    cieDiv.setAttribute('itemtype', 'http://schema.org/Organization');
    movieCie.append(cieDiv);

    const cieName = companies[i].name;
    const cieTitle = document.createElement('span');
    cieTitle.setAttribute('itemprop', 'name');
    cieTitle.innerText = cieName;
    cieDiv.append(cieTitle);

    if (companies[i].origin_country) {
      const cieCountry = companies[i].origin_country;
      const cieLand = document.createElement('span');
      cieLand.setAttribute('itemprop', 'countryOfOrigin');
      cieLand.setAttribute('itemscope', '');
      cieLand.setAttribute('itemtype', 'http://schema.org/Country');
      cieLand.innerText = ` (${cieCountry})`;
      cieDiv.append(cieLand);
    }
  }
  // Recommandations
  const recomDiv = document.getElementById('recommandations-container');
  const recommandations = data.recommendations.results;
  const recMovies = [];
  recommandations.forEach((entry) => {
    if (entry.poster_path) {
      recMovies.push(entry);
    }
  });
  const wid = 160 * recMovies.length;
  recomDiv.style.height = ('230px');
  recomDiv.style.width = (`${wid}px`);

  for (let i = 0; i < recMovies.length; i += 1) {
    const rPoster = recMovies[i].poster_path;
    const rTitle = recMovies[i].title;
    const rMovieId = recMovies[i].id;
    const rMovie = document.createElement('div');
    rMovie.style.width = '160px';
    rMovie.style.display = 'inline-block';
    rMovie.style.position = 'relative';
    recomDiv.append(rMovie);
    const rA = document.createElement('a');
    rA.id = rMovieId;
    rA.style.position = 'absolute';
    rA.style.left = '0';
    rA.style.right = '0';
    rA.style.zIndex = 999;
    rA.style.width = '160px';
    rA.style.height = '230px';
    rA.href = './movies.html';
    rMovie.append(rA);
    const rMoviePoster = document.createElement('img');
    rMoviePoster.setAttribute('src', `https://image.tmdb.org/t/p/w154${rPoster}`);
    rMoviePoster.setAttribute('alt', `Poster - ${rTitle}`);
    rMovie.append(rMoviePoster);
  }
};

// fetch des données
fetch(urlMovie)
  .then((reponse) => reponse.json())
  .then((transformation) => {
    const movieResult = transformation;
    console.log('Données (movie):', movieResult);
    affichageDetails(movieResult);


  })
  .catch((error) => {
    console.log('Error:', error);
  });

// LOCAL STORAGE
document.addEventListener('click', (myEvent) => { // quand on clique sur le titre d'un film, store l'id
localStorage.setItem('id', myEvent.target.id);
});
