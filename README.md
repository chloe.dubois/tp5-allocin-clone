# TP 5 : Allo ciné Clone

## Descriptif du projet 

Le projet était de réaliser un site web du type "allo ciné" où on peut rechercher des informations sur des films (durée, réalisateurs, synopsis, acteurs, etc.). Le site final contient deux pages html dynamiques (utilisation de javascript) : une pour l'accueil et la recherche, l'autre pour le détail des informations sur chaque film.

## Détails techniques

- Utilisation des données de l'API "The Movie Database" : https://developers.themoviedb.org ;
- Utilisation du framework css Bootstrap (css & js) ;
- Correction du javascript avec eslint selon la norme de Airbnb ;

## Clé API

La clé API n'est pas sur le dépôt Gitlab. Pour visionner le site, vous devez définir la constante "apikey" par votre propre clé, comme ci-dessous :

const apikey = '<<votre clé API>>';



